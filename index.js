console.log("Hello World!");

// Arithmetic Operators (+ - * / %)
let x = 1397;
let y = 7831;

let sum = x + y;
console.log("Result of addition operator: " + sum);

let difference = x - y;
console.log("Result of subtraction operator: " + difference);

let product = x * y;
console.log("Result of multiplication operator: " + product);

let quotient = x / y;
console.log("Result of division operator: " + quotient);

let remainder = y % x;
console.log("Result of modulo operator: " + remainder);

// Assignment Operators (=)
let assignmentNumber = 8;
assignmentNumber = assignmentNumber + 2;
console.log("Result of addition assignment operator: " +assignmentNumber);

// Shortcut for asssignment
assignmentNumber+=2;
console.log("Result of addition assignment operator: " +assignmentNumber);

assignmentNumber-=2;
console.log("Result of subtraction assignment operator: " +assignmentNumber);

assignmentNumber*=2;
console.log("Result of multiplication assignment operator: " +assignmentNumber);

assignmentNumber/=2;
console.log("Result of division assignment operator: " +assignmentNumber);

assignmentNumber%=2;
console.log("Result of modulo assignment operator: " +assignmentNumber);

// Multiple Operators and Parenthesis
let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas: " + mdas);

let pemdas = 1 + (2 - 3) * (4/5);
console.log("Result of pemdas: " + pemdas);

let z = 1;
 
let increment = ++z;
console.log("Result of pre-increment: " + increment);
console.log("Result of post-increment: " + z);

increment = z++; //wala muna plus 1
//increment = z++; //meron na plus 1
console.log("Result of post-increment: " + increment);

let decrement = --z;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of post-decrement: " + z);

decrement = z--;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of post-decrement: " + z);


// TYPE COERCION Automatic or implicit conversion of values from one data type to another

let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

let numE = true + 1
console.log(numE);
console.log(typeof numE);

// COMPARISON OPERATORS
	// Equality Operator (==) :NON-STRICT EQUAL
	console.log(1 == 1);
	console.log(1 == 2);
	console.log(1 == '1');

	console.log(0 == false);

	// Inequality Operator (!=) NON-STRICT INEQUAL
	console.log(1 != 1);
	console.log(1 != 2);

	// Equality Operator (===) :STRICT EQUAL (same value and data type)
	console.log(1 === "1");

	// Inequality Operator (!==) :STRICT INEQUAL (not same value and data type)
	console.log(1 !== "1");


// RELATIONAL OPERATORS (> < =)
// - checks whether one value is greater or less than to the other value

let a = 50;
let b = 65;

let isGreaterThan = a > b;
console.log(isGreaterThan);

let isLessThan = a < b;
console.log(isLessThan);

let isGTorEqual = a >= b;
console.log(isGTorEqual);

let isLTorEqual = a <= b;
console.log(isLTorEqual);

// LOGICAL OPERATORS (&& || !)

let isLegalAge = true;
let isRegistered = false;

let allRequirementsMet = isLegalAge && isRegistered;
console.log("Logical && Result: " + allRequirementsMet);

let someRequirementsMet = isLegalAge || isRegistered;
console.log("Logical || Result: " + someRequirementsMet)

let someRequirementsNotMet = !isRegistered;
console.log("Logical ! Result: " + someRequirementsNotMet)

